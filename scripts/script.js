// Нужно сделать:
// изменить логотип. Ready
//  чтобы работало нормально see more/hide information (скрывалась появлялась инфа).Ready.
// чтобы добавлялись новые  карточки.
// не работает drag and drop.
//
console.log("script connected")
const body = document.body;
const createVisit = document.getElementById('header__button-visit');
const popUpWindow = document.getElementById('popUp-block');
const doctorType = document.getElementById('doctor-type');
const popUpWindowClose = document.getElementsByClassName("popUp-block__close")[0];
const popUpForm = document.getElementsByClassName("pop-Up__form")[0];

const fio = document.getElementById("fio-popUp");
const nameVisit = document.getElementById("intent-popUp");
const age = document.getElementById("age-popUp");
const pressure = document.getElementById("pressure-popUp");
const weight = document.getElementById("weight-popUp");
const lastvisit = document.getElementById("lastvisit-popUp");
const diseases = document.getElementById("diseases-popUp");
const userMessage = document.getElementById("message-popUp");

let visitDesk = document.getElementById('visit-desk');

class Visit {
    constructor(fio,date,nameVisit,message,id){
        try {
            this.message = message;
            this.fio = fio;
            this.date = date;
            this.nameVisit = nameVisit;
            this.id = id
        } catch(e) {
            console.log(e);
        };
    };

    renderNewCard(){
        if(visitDesk.textContent === "No items have been added"){ //del text in #desk first time
            visitDesk.textContent = '';
        };

        visitDesk.appendChild(document.createElement('div')).className = `draggable `+this.id;
        let mainDiv = visitDesk.querySelector(`.${this.id}`);
        mainDiv.appendChild(document.createElement('a')).className = "card-close";
        mainDiv.appendChild(document.createElement('span')).className ="fio";
        mainDiv.appendChild(document.createElement('span')).className ="doctor";
        mainDiv.appendChild(document.createElement('a')).className = "card-moreinfo";
        mainDiv.getElementsByClassName("doctor")[0].textContent = this.constructor.name;
        mainDiv.getElementsByClassName("fio")[0].textContent = this.fio;


        if(!this.lastvisit && !this.pressure){    //therapist
            mainDiv.appendChild(document.createElement('span')).className ="date";
            mainDiv.getElementsByClassName("date")[0].textContent =`Register time: `+this.date;
            mainDiv.appendChild(document.createElement('span')).className ="nameVisit";
            mainDiv.getElementsByClassName("nameVisit")[0].textContent =`Intent: `+this.nameVisit;
            mainDiv.appendChild(document.createElement('span')).className ="age";
            mainDiv.getElementsByClassName("age")[0].textContent =`Years old: `+this.age;
            mainDiv.appendChild(document.createElement('span')).className ="message";
            mainDiv.getElementsByClassName("message")[0].textContent = this.message;

        } else if(!this.age && !this.lastvisit){  //Cardiologist
            mainDiv.appendChild(document.createElement('span')).className ="date";
            mainDiv.getElementsByClassName("date")[0].textContent =`Register time: `+this.date;
            mainDiv.appendChild(document.createElement('span')).className ="nameVisit";
            mainDiv.getElementsByClassName("nameVisit")[0].textContent =`Intent: `+this.nameVisit;
            mainDiv.appendChild(document.createElement('span')).className ="diseases";
            mainDiv.getElementsByClassName("diseases")[0].textContent =`Diseases: `+this.diseases;
            mainDiv.appendChild(document.createElement('span')).className ="weight";
            mainDiv.getElementsByClassName("weight")[0].textContent =`% of weight: `+ this.weight;
            mainDiv.appendChild(document.createElement('span')).className ="pressure";
            mainDiv.getElementsByClassName("pressure")[0].textContent =`Pressure: `+this.pressure;
            mainDiv.appendChild(document.createElement('span')).className ="message";
            mainDiv.getElementsByClassName("message")[0].textContent = this.message;

        } else if(!this.age && !this.pressure){  //Dentist
            mainDiv.appendChild(document.createElement('span')).className ="date";
            mainDiv.getElementsByClassName("date")[0].textContent =`Register time: `+this.date;
            mainDiv.appendChild(document.createElement('span')).className ="nameVisit";
            mainDiv.getElementsByClassName("nameVisit")[0].textContent =`Intent: `+this.nameVisit;
            mainDiv.appendChild(document.createElement('span')).className ="lastvisit";
            mainDiv.getElementsByClassName("lastvisit")[0].textContent =`Last visit to doctor: `+this.lastvisit;
            mainDiv.appendChild(document.createElement('span')).className ="message";
            mainDiv.getElementsByClassName("message")[0].textContent = this.message;
        };
    };
};




class Therapist extends Visit {
    constructor(nameVisit,age,fio,date,message,id){
        try {
            super(fio,date,nameVisit,message,id);
            this.age = age;
        } catch(e) {
            console.log(e);
        };
    };
};

class Dentist extends Visit {
    constructor(nameVisit,lastvisit,fio,date,message,id){
        try {
            super(fio,date,nameVisit,message,id);
            this.lastvisit = lastvisit;
        } catch(e) {
            console.log(e);
        };
    };
};

class Cardiologist extends Visit {
    constructor(nameVisit,pressure,weight,diseases,fio,date,message,id){
        try {
            super(fio,date,nameVisit,message,id);
            this.pressure = pressure;
            this.weight = weight;
            this.diseases = diseases;
        } catch(e) {
            console.log(e);
        };
    };
};

const toggleMenu = (el,cl) => {  //activ class trigger for popUp
    el.classList.toggle(cl);
};

createVisit.addEventListener('click', e => {
    e.stopPropagation();
    toggleMenu(popUpWindow,'popUp-deactive');
});

document.addEventListener('click', e => {
    let target = e.target;
    let its_popUp = target === popUpWindow || popUpWindow.contains(target);
    let its_createVisit = target === createVisit;
    let popUp_is_deactive = !popUpWindow.classList.contains('popUp-deactive');

    if (!its_popUp && !its_createVisit && popUp_is_deactive || target === popUpWindowClose) {
        toggleMenu(popUpWindow,'popUp-deactive');
    };
});

function runRandom(min, max) { //for random;p
    let randomNum = parseInt(Math.random() * (max - min) + min);
    return randomNum;
}

function cleanInput(){

    userMessage.value = "";
    for(let i = 2;i <= 8;++i){
        popUpForm.children[i].classList.add("input-deactive");
        popUpForm.children[i].value = "";
    };
};

doctorType.addEventListener("change",function(e){  //2-8 switcher #doctor-type
    cleanInput();
    if(this.value === "Therapist"){
        fio.classList.remove("input-deactive");
        nameVisit.classList.remove("input-deactive");
        age.classList.remove("input-deactive");
    } else if(this.value === "Dentist"){
        fio.classList.remove("input-deactive");
        nameVisit.classList.remove("input-deactive");
        lastvisit.classList.remove("input-deactive");
    } else if(this.value === "Cardiologist"){
        fio.classList.remove("input-deactive");
        nameVisit.classList.remove("input-deactive");
        weight.classList.remove("input-deactive");
        age.classList.remove("input-deactive");
        pressure.classList.remove("input-deactive");
        diseases.classList.remove("input-deactive");
    };
});

getTimeNow=()=>{
    let date = new Date();
    let monthes = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
    let day = date.getDate(),
        month = monthes[date.getMonth()],
        year = date.getFullYear(),
        hours = date.getHours(),
        minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes();
    return (day + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes);
};

function submitPopUp(){  //create new object Visit
    let newVisit = {};
    let visitId = `visit${runRandom(1,1000)}_${runRandom(1,1000)}`;
    if(doctorType.children[3].selected && fio.value.length >= 1 &&  nameVisit.value.length >= 1 && diseases.value.length >= 1 && weight.value.length >= 1 && pressure.value.length >= 1 && nameVisit.value.length >= 1){
        newVisit = new Cardiologist(nameVisit.value,pressure.value,weight.value,diseases.value,fio.value,getTimeNow(),userMessage.value,visitId);
    }else if (doctorType.children[2].selected && !diseases.value.length >= 1 &&lastvisit.value.length >= 1 && fio.value.length >= 1 && nameVisit.value.length >= 1){
        newVisit = new Dentist(nameVisit.value,lastvisit.value,fio.value,getTimeNow(),userMessage.value,visitId);
    } else if (doctorType.children[1].selected && !lastvisit.value.length >= 1 && !diseases.value.length >= 1 && fio.value.length >= 1 && nameVisit.value.length >= 1 && age.value.length >= 1){
        newVisit = new Therapist(nameVisit.value,age.value,fio.value,getTimeNow(),userMessage.value,visitId);
    }else{
        return newVisit;
    };

    localStorage.setItem(visitId , JSON.stringify(newVisit)); //add new obj in localstorage
    doctorType.children[0].selected = "true";
    cleanInput();
    toggleMenu(popUpWindow,'popUp-deactive');
    newVisit.renderNewCard();
};

document.body.onclick= function(e){  //close this card and clean in local storage
    e = window.event? event.srcElement: e.target;
    let parentClickElement = event.srcElement.parentElement;
    if(e.className && e.className.indexOf('card-close')!==-1){
        localStorage.removeItem(parentClickElement.classList[1]); //delElement in local storage
        parentClickElement.remove();  //del in DOM
        if(!visitDesk.hasChildNodes()){visitDesk.textContent = "No items have been added"};
    }else if(e.className && e.className.indexOf('card-moreinfo')!==-1){
        toggleMenu(parentClickElement,'carddoctor-big');
        toggleMenu(parentClickElement.children[0],'card-closehide');
    }
}

createCardWithLocalStorage = (obj,nameObj) => { //take obj in storage and create card in visit-desk
    if(obj.lastvisit === undefined && obj.diseases === undefined){
        nameObj = new Therapist(obj.nameVisit,obj.age,obj.fio,obj.date,obj.message,obj.id);
    }else if(obj.diseases === undefined && obj.age === undefined){
        nameObj = new Dentist(obj.nameVisit,obj.lastvisit,obj.fio,obj.date,obj.message,obj.id);
    } else {
        nameObj = new Cardiologist(obj.nameVisit,obj.pressure,obj.weight,obj.diseases,obj.fio,obj.date,obj.message,obj.id);
    };
    nameObj.renderNewCard();
};
//start local storege
const ready = function() {  //find all in localstorage
    let lsLen = localStorage.length
    if(lsLen > 0 ){
        for(let i = 0; i < lsLen; i++){
            let key = localStorage.key(i);
            let retrievedObject = localStorage.getItem(key);
            createCardWithLocalStorage(JSON.parse(retrievedObject),JSON.parse(retrievedObject).id);
        };
    };
};
//on load page run localstorage func
document.addEventListener("DOMContentLoaded", ready);
//start drag-and-drop
document.addEventListener('mousedown',(e)=>{
    draggable = e.target;
    if (!draggable.classList.contains('draggable')) return; //if it not draggable class
    if (draggable.style.transform) {
        const transforms = draggable.style.transform;
        const transformX = parseFloat(transforms.split('(')[1].split(',')[0]);
        const transformY = parseFloat(transforms.split('(')[1].split(',')[1]);
        draggable.mousePositionX = e.clientX - transformX;
        draggable.mousePositionY = e.clientY - transformY;
    } else {
        draggable.mousePositionX = e.clientX;
        draggable.mousePositionY = e.clientY;
    }
    draggable.addEventListener('mousemove',move);

    document.addEventListener('mouseup', e => {
        if (!draggable.classList.contains('draggable')) return;
        draggable.removeEventListener('mousemove',move);
    });
    function move(e) {
        draggable.style.transform = `translate(${e.clientX - draggable.mousePositionX}px,${e.clientY - draggable.mousePositionY}px)`;
    }
});

